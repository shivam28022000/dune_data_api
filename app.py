from fastapi import FastAPI
import os
from pydantic import BaseModel
from duneapi.api import DuneAPI
from duneapi.types import Network, QueryParameter, DuneRecord, DuneQuery
from duneapi.util import open_query


def fetch_records(dune: DuneAPI):
    sample_query = DuneQuery.from_environment(
        raw_sql=open_query("dune.sql"),
        name="Sample Query",
        network=Network.MAINNET,
        parameters=[
            QueryParameter.number_type("IntParam", 10),
            QueryParameter.text_type("TextParam", "aba"),
        ],
    )
    return dune.fetch(sample_query)

app = FastAPI()



class User_credentials(BaseModel):
    DUNE_USER: str
    DUNE_PASSWORD: str
    DUNE_QUERY_ID: str


@app.post("/usr/")
async def root(usr:User_credentials):
    
    print("check 1",usr)
    os.environ['DUNE_USER'] = usr.DUNE_USER
    print("check 2")
    os.environ['DUNE_PASSWORD'] = usr.DUNE_PASSWORD
    print("check 3")
    os.environ['DUNE_QUERY_ID'] = usr.DUNE_QUERY_ID
    print("check 4") 
    dune_connection = DuneAPI.new_from_environment()
    records = fetch_records(dune_connection)
    print(records)
    return {"data": records}